#include "GlutonyBall.h"


GlutonyBall::GlutonyBall()
{
	m_SinType = ST_GLUTONY;
}

GlutonyBall::~GlutonyBall()
{
}

bool GlutonyBall::init()
{
	if (!Node::init())
		return false;


	m_BallSprite = cocos2d::Sprite::create("Images/spr_sins_glutony.png");
	auto destroyAnimation = initAnimation("glutony", GLUTONY_FRAME_COUNT, 0.15);
	m_dieAnimation = cocos2d::Animate::create(destroyAnimation);
	Sin::init();

	this->addChild(m_BallSprite);
	return true;
}

void GlutonyBall::destroySin()
{
}
