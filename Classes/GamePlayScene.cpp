#include "GamePlayScene.h"
#include "Config.h"

cocos2d::Scene* GamePlayScene::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = GamePlayScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

bool GamePlayScene::init()
{
	if (!Node::init())
	{
		return false;
	}
	//add animation frame to the cocos sprite frame cache
	auto spriteFrameCache = cocos2d::SpriteFrameCache::getInstance();
	spriteFrameCache->addSpriteFramesWithFile("Animations/anim_sins_destroy_envy_plist.plist", "Animations/anim_sins_destroy_envy_img.png");
	spriteFrameCache->addSpriteFramesWithFile("Animations/anim_sins_destroy_glutony_plist.plist", "Animations/anim_sins_destroy_glutony_img.png");
	spriteFrameCache->addSpriteFramesWithFile("Animations/anim_sins_destroy_greed_plist.plist", "Animations/anim_sins_destroy_greed_img.png");
	spriteFrameCache->addSpriteFramesWithFile("Animations/anim_sins_destroy_lust_plist.plist", "Animations/anim_sins_destroy_lust_img.png");
	spriteFrameCache->addSpriteFramesWithFile("Animations/anim_sins_destroy_pride_plist.plist", "Animations/anim_sins_destroy_pride_img.png");
	spriteFrameCache->addSpriteFramesWithFile("Animations/anim_sins_destroy_sloth_plist.plist", "Animations/anim_sins_destroy_sloth_img.png");
	spriteFrameCache->addSpriteFramesWithFile("Animations/anim_sins_destroy_wrath_plist.plist", "Animations/anim_sins_destroy_wrath_img.png");

	auto visibleSize = Director::getInstance()->getVisibleSize();

	auto backgroundImage = Sprite::create("Images/spr_background_gameplay_vanquishment.png");
	backgroundImage->setScale((float)DES_WIDTH /(float)RETINA_WIDTH);
	backgroundImage->setPosition(visibleSize.width / 2, visibleSize.height / 2);
	this->addChild(backgroundImage);

	m_GameBoard = Board::create();
	this->addChild(m_GameBoard);

	return true;
}

