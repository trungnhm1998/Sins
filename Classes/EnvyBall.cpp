#include "EnvyBall.h"



EnvyBall::EnvyBall()
{
	m_SinType = ST_ENVY;
}


EnvyBall::~EnvyBall()
{
}

bool EnvyBall::init()
{
	if (!Node::init())
		return false;


	m_BallSprite = cocos2d::Sprite::create("Images/spr_sins_envy.png");
	auto destroyAnimation = initAnimation("envy", ENVY_FRAME_COUNT, 0.15);
	m_dieAnimation = cocos2d::Animate::create(destroyAnimation);
	Sin::init();

	this->addChild(m_BallSprite);

	return true;
}

void EnvyBall::destroySin()
{
}

