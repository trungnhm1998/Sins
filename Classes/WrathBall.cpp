#include "WrathBall.h"



WrathBall::WrathBall()
{
	m_SinType = ST_WRATH;
}


WrathBall::~WrathBall()
{
}

bool WrathBall::init()
{
	if (!Node::init())
		return false;
	

	m_BallSprite = cocos2d::Sprite::create("Images/spr_sins_wrath.png");
	

	Sin::init();

	
	this->addChild(m_BallSprite);

	

	return true;
}

void WrathBall::destroySin()
{
	auto destroyAnimation = initAnimation("wrath", WRATH_FRAME_COUNT, 0.15);
	m_dieAnimation = cocos2d::Animate::create(destroyAnimation);

	m_BallSprite->runAction(cocos2d::Sequence::create(m_dieAnimation, cocos2d::RemoveSelf::create(true),nullptr));
}

