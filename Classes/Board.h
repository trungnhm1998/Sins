#pragma once
#include "cocos2d.h"
#include "Sin.h"
#include "EnvyBall.h"
#include "GlutonyBall.h"
#include "GreedBall.h"
#include "LustBall.h"
#include "PrideBall.h"
#include "SlothBall.h"
#include "WrathBall.h"

#define BOARD_SPRITE_NAME "Images/spr_gameplay_board_background.png"
#define BOARD_WIDTH 9
#define BOARD_HEIGHT 9
#define CELL_SIZE 137


enum CELL_STATE
{
	CS_HAVE_SIN = 0,
	CS_EMPTY,
	CS_SPAWN_NEXT
};

class Board : public cocos2d::Node
{
public:
	Board();
	~Board();
	bool init() override;

	void mouseDown(cocos2d::Event* event);
	CREATE_FUNC(Board);

private:
	void checkBoard(int row, int col);
	void finishedMoving();
	void finishedScoring();
	bool m_IsMoving;
	bool m_IsScoring;
	
	Sin* m_Seleted;


	
	void spawnBall();
	bool checkDirection(int row, int col);

	
	//adapted Cell size with screen
	float m_CellSize;
	cocos2d::Sprite* m_BoardSprite;

	

	Sin* m_Board[BOARD_WIDTH*BOARD_HEIGHT];
	CELL_STATE m_BoardState[BOARD_WIDTH*BOARD_HEIGHT];

};


