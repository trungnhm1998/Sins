#include "Board.h"
#include "Config.h"
#include "Graph.h"
#include <png/include/win32/png.h>



Board::Board()
{
	m_IsMoving = false;
	m_IsScoring = false;
	m_Seleted = nullptr;
	memset(m_Board, NULL, sizeof(m_Board));
}


Board::~Board()
{
	//for (int i = 0; i < BOARD_WIDTH*BOARD_HEIGHT; i++)
	//{
	//	if (m_Board[i] != nullptr)
	//	{
	//		delete m_Board[i];
	//	}
	//}
}

bool Board::init()
{
	if (!Node::init())
	{
		return false;
	}

	//init graph for BFS

	//set default state for entire board
	for (int i = 0; i < BOARD_WIDTH*BOARD_HEIGHT; i++)
	{
		m_BoardState[i] = CS_EMPTY;
	}


	//mouse Event begin
	auto mouseEventListener = cocos2d::EventListenerMouse::create();
	mouseEventListener->onMouseDown = CC_CALLBACK_1(Board::mouseDown, this);

	//add to dispathcher
	_eventDispatcher->addEventListenerWithSceneGraphPriority(mouseEventListener, this);
	//mouse Event End

	auto visibleSize = cocos2d::Director::getInstance()->getVisibleSize();

	//cocos2d::log("%f-%f", visibleSize.width, visibleSize.height);

	m_CellSize = CELL_SIZE*((float)DES_WIDTH / (float)RETINA_WIDTH);
	m_BoardSprite = cocos2d::Sprite::create("Images/spr_gameplay_board_background.png");
	m_BoardSprite->setScale((float)DES_WIDTH / (float)RETINA_WIDTH, (float)DES_HEIGHT / (float)RETINA_HEIGHT);
	m_BoardSprite->setPosition(visibleSize.width / 2, visibleSize.height / 2);
	this->addChild(m_BoardSprite);

	//memset(m_Board, nullptr, BOARD_WIDTH*BOARD_HEIGHT * sizeof(Sin));

	return true;
}

void Board::mouseDown(cocos2d::Event* event)
{
	cocos2d::EventMouse* mouseEvent = (cocos2d::EventMouse*)event;
	float cursorX = mouseEvent->getCursorX();
	float cursorY = mouseEvent->getCursorY();
	
	static cocos2d::Vec2 src;
	static cocos2d::Vec2 des;

	if(mouseEvent->getMouseButton() == 0&& !m_IsMoving)
	{
		//cocos2d::log("LEFT");
		if (floorf(cursorX) >= floorf(m_BoardSprite->getBoundingBox().getMinX()) && floorf(cursorX) < floorf(m_BoardSprite->getBoundingBox().getMaxX()) &&
			floorf(cursorY) >= floorf(m_BoardSprite->getBoundingBox().getMinY()) && floorf(cursorY) < floorf(m_BoardSprite->getBoundingBox().getMaxY()))
		{
			int boardCol = (int)(floorf(cursorX - m_BoardSprite->getBoundingBox().getMinX()) / floorf(m_CellSize));
			int boardRow = (int)(floorf(cursorY - m_BoardSprite->getBoundingBox().getMinY()) / floorf(m_CellSize));
			src.x = boardCol;
			src.y = boardRow;
			if (m_BoardState[BOARD_WIDTH * boardRow + boardCol] != CS_EMPTY)
			{
				cocos2d::log("have ball");
				m_Seleted = m_Board[BOARD_WIDTH * boardRow + boardCol];
			}
			else if(m_BoardState[BOARD_WIDTH * boardRow + boardCol] == CS_EMPTY)
			{
				cocos2d::log("no ball");
				m_Seleted = nullptr;
			}
		}

	}
	if (mouseEvent->getMouseButton() == 1 && !m_IsMoving)
	{
		//cocos2d::log("RIGHT");
		if (floorf(cursorX) >= floorf(m_BoardSprite->getBoundingBox().getMinX()) && floorf(cursorX) < floorf(m_BoardSprite->getBoundingBox().getMaxX()) &&
			floorf(cursorY) >= floorf(m_BoardSprite->getBoundingBox().getMinY()) && floorf(cursorY) < floorf(m_BoardSprite->getBoundingBox().getMaxY()))
		{
			int boardCol = (int)(floorf(cursorX - m_BoardSprite->getBoundingBox().getMinX()) / floorf(m_CellSize));
			int boardRow = (int)(floorf(cursorY - m_BoardSprite->getBoundingBox().getMinY()) / floorf(m_CellSize));
			des.x = boardCol;
			des.y = boardRow;
				
			if (m_Seleted != nullptr && (des.x != src.x || des.y != src.y))
			{
				
				if (m_BoardState[BOARD_WIDTH*(int)des.y + (int)des.x] == CS_EMPTY)
				{
					Graph m_Graph(9, 9);

					//vector path
					auto path = m_Graph.BFS(src.x, src.y, des.x, des.y, m_BoardState);
					cocos2d::log("%d", path.size());

					float boardOffsetX = m_BoardSprite->getBoundingBox().getMinX();
					float boardOffsetY = m_BoardSprite->getBoundingBox().getMinY();

					cocos2d::Vector<cocos2d::FiniteTimeAction*> movePath;
					for (std::vector<cocos2d::Vec2>::iterator i = path.begin(); i != path.end(); ++i)
					{
						auto moveTo = cocos2d::MoveTo::create(0.2, cocos2d::Vec2((boardOffsetX + m_CellSize / 2) + ((*i).x * m_CellSize),
							(boardOffsetY + m_CellSize / 2) + ((*i).y * m_CellSize)));
						auto delay = cocos2d::DelayTime::create(0.2f);
						movePath.pushBack(moveTo);
						movePath.pushBack(delay);
					}

					//after moving the user can select sin and move it again
					m_Seleted->m_BallSprite->stopAllActions();
					auto finishMovingCallBack = cocos2d::CallFunc::create([this]() {
						this->finishedMoving();
					});
					movePath.pushBack(finishMovingCallBack);

					//after done moving will scan the board for scroing
					
					auto checkDirection = cocos2d::CallFunc::create([=]() {
						this->checkDirection(boardRow,boardCol);
					});
					movePath.pushBack(checkDirection);

					//auto moveTo = cocos2d::MoveTo::create(2.f, cocos2d::Vec2((boardOffsetX + m_CellSize / 2) + (des.x * m_CellSize),
						//(boardOffsetY + m_CellSize / 2) + (des.y * m_CellSize)));

					m_IsMoving = true;
					m_Seleted->m_BallSprite->runAction(cocos2d::Sequence::create(movePath));

					m_BoardState[BOARD_WIDTH*(int)(src.y) + (int)(src.x)] = CS_EMPTY;
					m_Board[BOARD_WIDTH*(int)(src.y) + (int)(src.x)] = NULL;
					src.x = boardCol;
					src.y = boardRow;
					m_BoardState[BOARD_WIDTH*boardRow + boardCol] = CS_HAVE_SIN;
					m_Board[BOARD_WIDTH*boardRow + boardCol] = m_Seleted;
				}

			}
			
		}
	}
	if (mouseEvent->getMouseButton() == 2 && !m_IsMoving && !m_IsScoring)
	{
		//cocos2d::log("src[%f-%f] des[%f-%f]",src.x,src.y,des.x,des.y);

		if (floorf(cursorX) >= floorf(m_BoardSprite->getBoundingBox().getMinX()) && floorf(cursorX) < floorf(m_BoardSprite->getBoundingBox().getMaxX()) &&
			floorf(cursorY) >= floorf(m_BoardSprite->getBoundingBox().getMinY()) && floorf(cursorY) < floorf(m_BoardSprite->getBoundingBox().getMaxY()))
		{
			int boardCol = (int)(floorf(cursorX - m_BoardSprite->getBoundingBox().getMinX()) / floorf(m_CellSize));
			int boardRow = (int)(floorf(cursorY - m_BoardSprite->getBoundingBox().getMinY()) / floorf(m_CellSize));


			if (boardCol < BOARD_WIDTH && boardRow < BOARD_HEIGHT && m_BoardState[BOARD_WIDTH* boardRow + boardCol] != CS_HAVE_SIN)
			{
				m_BoardState[BOARD_WIDTH* boardRow + boardCol] = CS_HAVE_SIN;
				m_Board[BOARD_WIDTH* boardRow + boardCol] = WrathBall::create();
				m_Board[BOARD_WIDTH* boardRow + boardCol]->initSin((m_BoardSprite->getBoundingBox().getMinX() + ((m_CellSize / 2) + ((m_CellSize)*boardCol))),
					(m_BoardSprite->getBoundingBox().getMinY() + (m_CellSize / 2) + ((m_CellSize)*boardRow)));
				this->addChild(m_Board[BOARD_WIDTH* boardRow + boardCol]);

				//check ball
				checkBoard(boardRow, boardCol);

			}
	}





		//cocos2d::log("%f %f-%f board: %f-%f-%f-%f cell: %d-%d",m_CellSize, cursorX, cursorY,
		//	m_BoardSprite->getBoundingBox().getMinX(),
		//	m_BoardSprite->getBoundingBox().getMinY(),
		//	m_BoardSprite->getBoundingBox().getMaxX(),
		//	m_BoardSprite->getBoundingBox().getMaxY(),
		//	col, row);
	}

}

void Board::checkBoard(int row, int col)
{
	if (checkDirection(row, col))
	{
		//destroy current sin ball
		m_BoardState[BOARD_WIDTH*row + col] = CS_EMPTY;
		m_Board[BOARD_WIDTH*row + col]->destroySin();
	}
}

void Board::finishedMoving()
{
	m_IsMoving = false;
}

void Board::finishedScoring()
{
	m_IsScoring = false;
}


void Board::spawnBall()
{

}

bool Board::checkDirection(int row, int col)
{
	bool enoughBallFlag = false;

	int leftCount = 0;
	int rightCount = 0;

	int topCount = 0;
	int bottomCount = 0;

	int topLeftCount = 0;
	int bottomRightCount = 0;

	int topRightCount = 0;
	int bottomLeftCount = 0;

	//start from the next right cell
	int colOffset = col;
	int rowOffset = row;

	//check Horizontal Right ->
	colOffset = col + 1;
	if (colOffset < BOARD_WIDTH)
	{
		while (m_BoardState[BOARD_WIDTH*row + colOffset] == CS_HAVE_SIN
			&& m_Board[BOARD_WIDTH*row + colOffset]->getSin() == m_Board[BOARD_WIDTH*row + col]->getSin()
			&& colOffset < BOARD_WIDTH)
		{
			rightCount++;
			//check if still on the current row
			colOffset++;

		}
	}

	//chec Horizontal Left <-
	colOffset = col - 1;
	if (colOffset >= 0)
	{
		while (m_BoardState[BOARD_WIDTH*row + colOffset] == CS_HAVE_SIN
			&& m_Board[BOARD_WIDTH*row + colOffset]->getSin() == m_Board[BOARD_WIDTH*row + col]->getSin()
			&& colOffset >= 0)
		{
			leftCount++;
			colOffset--;

		}
	}

	//check vertical top
	rowOffset = row + 1;
	if (rowOffset < BOARD_HEIGHT)
	{
		while (rowOffset < BOARD_HEIGHT && m_BoardState[BOARD_WIDTH*rowOffset + col] == CS_HAVE_SIN
			&& m_Board[BOARD_WIDTH*rowOffset + col]->getSin() == m_Board[BOARD_WIDTH*row + col]->getSin())
		{
			topCount++;
			rowOffset++;
		}
	}

	//check vertical bottom
	rowOffset = row - 1;
	if (rowOffset >= 0)
	{
		while (rowOffset >= 0 &&m_BoardState[BOARD_WIDTH*rowOffset + col] == CS_HAVE_SIN
			&& m_Board[BOARD_WIDTH*rowOffset + col]->getSin() == m_Board[BOARD_WIDTH*row + col]->getSin())
			
		{
			bottomCount++;
			rowOffset--;
		}
	}

	//check top left
	rowOffset = row + 1;
	colOffset = col - 1;
	if (rowOffset < BOARD_HEIGHT && colOffset >= 0)
	{
		while (m_BoardState[BOARD_WIDTH*rowOffset + colOffset] == CS_HAVE_SIN
			&& m_Board[BOARD_WIDTH*rowOffset + colOffset]->getSin() == m_Board[BOARD_WIDTH*row + col]->getSin()
			&& rowOffset < BOARD_HEIGHT && colOffset >= 0)
		{
			topLeftCount++;
			rowOffset++;
			colOffset--;
		}
	}

	//check bottomRight
	rowOffset = row - 1;
	colOffset = col + 1;
	if (rowOffset >= 0 && colOffset < BOARD_WIDTH)
	{
		while (rowOffset >= 0 && colOffset < BOARD_WIDTH && 
			m_BoardState[BOARD_WIDTH*rowOffset + colOffset] == CS_HAVE_SIN
			&& m_Board[BOARD_WIDTH*rowOffset + colOffset]->getSin() == m_Board[BOARD_WIDTH*row + col]->getSin())
		{
			bottomRightCount++;
			rowOffset--;
			colOffset++;
		}
	}

	//check top right
	rowOffset = row + 1;
	colOffset = col + 1;
	if(rowOffset < BOARD_HEIGHT && colOffset < BOARD_WIDTH)
	{
		while (rowOffset < BOARD_HEIGHT && colOffset < BOARD_WIDTH &&
			m_BoardState[BOARD_WIDTH*rowOffset + colOffset] == CS_HAVE_SIN
			&& m_Board[BOARD_WIDTH*rowOffset + colOffset]->getSin() == m_Board[BOARD_WIDTH*row + col]->getSin())
		{
			topRightCount++;
			rowOffset++;
			colOffset++;
		}

	}

	//check bottom left
	rowOffset = row - 1;
	colOffset = col - 1;
	if (rowOffset >= 0 && colOffset >= 0)
	{
		while (rowOffset < BOARD_HEIGHT && colOffset < BOARD_WIDTH && 
			rowOffset >= 0 && colOffset >= 0 &&
			m_BoardState[BOARD_WIDTH*rowOffset + colOffset] == CS_HAVE_SIN
			&& m_Board[BOARD_WIDTH*rowOffset + colOffset]->getSin() == m_Board[BOARD_WIDTH*row + col]->getSin())
		{
			bottomLeftCount++;
			rowOffset--;
			colOffset--;

		}
	}

	//if horizontal have more than 5 balls with same type
	if (leftCount + rightCount >= 4)
	{
		enoughBallFlag = true;
		//destroy ball on left
		for (int i = col; i >= col - leftCount; i--)
		{
			m_BoardState[BOARD_WIDTH*row + i] = CS_EMPTY;
			m_Board[BOARD_WIDTH*row + i]->destroySin();
		}

		//destroy ball on right
		for (int i = col; i <= col + rightCount; i++)
		{
			m_BoardState[BOARD_WIDTH*row + i] = CS_EMPTY;
			m_Board[BOARD_WIDTH*row + i]->destroySin();
		}
	}


	//if vertical have more than 5 balls with same type
	if (topCount + bottomCount >= 4)
	{
		enoughBallFlag = true;
		//destroy ball on top first
		for (int i = row; i <= row + topCount; i++)
		{
			m_BoardState[BOARD_WIDTH*i + col] = CS_EMPTY;
			m_Board[BOARD_WIDTH*i + col]->destroySin();
		}

		//destroy ball on bottom
		for (int i = row; i >= row - bottomCount; i--)
		{
			m_BoardState[BOARD_WIDTH*i + col] = CS_EMPTY;
			m_Board[BOARD_WIDTH*i + col]->destroySin();
		}

	}

	//check if the top left and bottom right cross have more than 5 balls
	if(topLeftCount+ bottomRightCount >=4)
	{
		enoughBallFlag = true;
		//destroy top left balls
		for (int r = row, c = col ; r <= row+topLeftCount && c>= col-topLeftCount; r++,c--)
		{
			m_BoardState[BOARD_WIDTH*r + c] = CS_EMPTY;
			m_Board[BOARD_WIDTH*r + c]->destroySin();
		}

		//destroy bottom right balls
		for (int r = row, c = col; r >= row-bottomRightCount && c <= col+bottomRightCount ; r--, c++)
		{
			m_BoardState[BOARD_WIDTH*r + c] = CS_EMPTY;
			m_Board[BOARD_WIDTH*r + c]->destroySin();
		}
	}

	//check if the top right and bottom left corss have more than 5 balls
	if(topRightCount + bottomLeftCount >=4)
	{
		enoughBallFlag = true;
		//destroy top right
		for (int r = row, c = col; r <= row + topRightCount && c <= col + topRightCount; r++, c++)
		{
			m_BoardState[BOARD_WIDTH*r + c] = CS_EMPTY;
			m_Board[BOARD_WIDTH*r + c]->destroySin();
		}

		//destroy bottom left
		for (int r = row, c = col; r >= row - bottomLeftCount && c >= col - bottomLeftCount; r--, c--)
		{
			m_BoardState[BOARD_WIDTH*r + c] = CS_EMPTY;
			m_Board[BOARD_WIDTH*r + c]->destroySin();
		}
	}

	return enoughBallFlag;
}
