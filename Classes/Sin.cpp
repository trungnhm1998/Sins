#include "Sin.h"



Sin::Sin()
{
	m_BallSprite = nullptr;
	m_SinType = ST_ENVY;
}


Sin::~Sin()
{
}

bool Sin::init()
{
	if (!cocos2d::Node::init())
	{
		return false;
	}

	m_BallSprite->setScale((float)DES_WIDTH / (float)RETINA_WIDTH, (float)DES_HEIGHT / (float)RETINA_HEIGHT);


	return true;
}

void Sin::initSin(float x, float y)
{

	m_BallSprite->setPosition(x, y);
}

int Sin::genSin()
{
	return rand() % MAX_SIN_SIZE;
}

SIN_TYPE Sin::getSin()
{
	return m_SinType;
}

void Sin::destroySin()
{

}


cocos2d::Animation* Sin::initAnimation(const char* animFrameName, int frameCount, float animDelay)
{
	cocos2d::Vector<cocos2d::SpriteFrame*> Sprites;

	for (int i = 1; i <= frameCount; i++)
	{
		char frameName[100];
		sprintf(frameName, "anim_sins_destroy_%s_%02d.png", animFrameName, i);
		cocos2d::SpriteFrame* frame = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName(frameName);

		Sprites.pushBack(frame);
	}

	return cocos2d::Animation::createWithSpriteFrames(Sprites, animDelay);
}