#include "PrideBall.h"



PrideBall::PrideBall()
{
	m_SinType = ST_PRIDE;
}


PrideBall::~PrideBall()
{
}

bool PrideBall::init()
{
	if (!Node::init())
		return false;

	m_BallSprite = cocos2d::Sprite::create("Images/spr_sins_pride.png");
	auto destroyAnimation = initAnimation("pride", PRIDE_FRAME_COUNT, 0.15);
	m_dieAnimation = cocos2d::Animate::create(destroyAnimation);
	Sin::init();

	this->addChild(m_BallSprite);

	return true;
}

void PrideBall::destroySin()
{
}

