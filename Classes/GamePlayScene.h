#pragma once
#include "cocos2d.h"
#include "Board.h"

#define GLUTONY_FRAME_COUNT 22
#define ENVY_FRAME_COUNT 20
#define GREED_FRAME_COUNT 17
#define LUST_FRAME_COUNT 17
#define PRIDE_FRAME_COUNT 15
#define SLOTH_FRAME_COUNT 14
#define WRATH_FRAME_COUNT 48

using namespace cocos2d;

class GamePlayScene : public Node
{
public:
	static cocos2d::Scene* createScene();

	bool init() override;
	CREATE_FUNC(GamePlayScene);

private:
	Board* m_GameBoard;
};

