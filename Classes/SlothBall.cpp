#include "SlothBall.h"



SlothBall::SlothBall()
{
	m_SinType = ST_SLOTH;
}


SlothBall::~SlothBall()
{
}

bool SlothBall::init()
{
	if (!Node::init())
		return false;

	m_BallSprite = cocos2d::Sprite::create("Images/spr_sins_sloth.png");
	auto destroyAnimation = initAnimation("sloth", SLOTH_FRAME_COUNT, 0.15);
	m_dieAnimation = cocos2d::Animate::create(destroyAnimation);
	Sin::init();

	this->addChild(m_BallSprite);

	return true;
}

void SlothBall::destroySin()
{
}

