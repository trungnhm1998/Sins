#include "LustBall.h"



LustBall::LustBall()
{
	m_SinType = ST_LUST;
}


LustBall::~LustBall()
{
}

bool LustBall::init()
{
	if (!Node::init())
		return false;

	m_BallSprite = cocos2d::Sprite::create("Images/spr_sins_lust.png");
	auto destroyAnimation = initAnimation("lust", LUST_FRAME_COUNT, 0.15);
	m_dieAnimation = cocos2d::Animate::create(destroyAnimation);
	Sin::init();

	this->addChild(m_BallSprite);

	return true;
}

void LustBall::destroySin()
{
}

