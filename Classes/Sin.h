#pragma once
#include "cocos2d.h"
#include "Config.h"
#define MAX_SIN_SIZE 7

#define GLUTONY_FRAME_COUNT 22
#define ENVY_FRAME_COUNT 20
#define GREED_FRAME_COUNT 17
#define LUST_FRAME_COUNT 17
#define PRIDE_FRAME_COUNT 15
#define SLOTH_FRAME_COUNT 14
#define WRATH_FRAME_COUNT 48

enum SIN_TYPE
{
	ST_ENVY = 0,
	ST_GLUTONY,
	ST_GREED,
	ST_LUST,
	ST_PRIDE,
	ST_SLOTH,
	ST_WRATH,
	ST_DEFAULT//DEFAULT will use current m_SinType
};

class Sin : public cocos2d::Node
{
public:
	Sin();
	~Sin();

	bool init() override;
	void initSin( float x, float y);
	static cocos2d::Animation* initAnimation(const char* animFrameName, int frameCount, float animDelay);

	static int genSin();
	SIN_TYPE getSin();
	virtual void destroySin();

	CREATE_FUNC(Sin);


	cocos2d::Sprite* m_BallSprite;
protected:
	SIN_TYPE m_SinType;
	cocos2d::Animate* m_dieAnimation;

};

