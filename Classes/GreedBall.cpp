#include "GreedBall.h"



GreedBall::GreedBall()
{
	m_SinType = ST_GREED;
}


GreedBall::~GreedBall()
{
}

bool GreedBall::init()
{
	if (!Node::init())
		return false;

	m_BallSprite = cocos2d::Sprite::create("Images/spr_sins_greed.png");
	auto destroyAnimation = initAnimation("greed", GREED_FRAME_COUNT, 0.15);
	m_dieAnimation = cocos2d::Animate::create(destroyAnimation);
	Sin::init();

	this->addChild(m_BallSprite);

	return true;
}

void GreedBall::destroySin()
{
}

