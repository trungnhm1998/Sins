#include "Graph.h"


Graph::Graph(unsigned int row, unsigned int col)
{
	m_Col = col;
	m_Row = row;
	m_CellCount = m_Row * m_Col;
	m_Cells = new Cell*[m_CellCount];

	//Init all the Node/Cell/Vertex on the graph to be m_Col*m_Row board
	for (int i = 0; i < m_Row; i++)
	{
		for (int j = 0; j < m_Col; j++)
		{
			m_Cells[m_Col*i + j] = new Cell(i, j);
		}
	}

	//every cell have their neighbour. add neighbour to current cell
	for (int i = 0; i < m_Row; i++)
	{
		for (int j = 0; j < m_Col; j++)
		{
			if (j - 1 >= 0)
			{
				m_Cells[m_Col*i + j]->addNeighbour(m_Cells[m_Col*i + (j - 1)]);
			}
			if (j + 1 < m_Col)
			{
				m_Cells[m_Col*i + j]->addNeighbour(m_Cells[m_Col*i + (j + 1)]);
			}
			if (i - 1 >= 0)
			{
				m_Cells[m_Col*i + j]->addNeighbour(m_Cells[m_Col*(i - 1) + j]);
			}
			if (i + 1 < m_Row)
			{
				m_Cells[m_Col*i + j]->addNeighbour(m_Cells[m_Col*(i + 1) + j]);
			}
		}
	}

}


Graph::~Graph()
{
	for (int i = 0; i < m_Row; i++)
	{
		for (int j = 0; j < m_Col; j++)
		{
			delete m_Cells[m_Row*i + j];
		}
	}
	delete[] m_Cells;
}

std::vector<cocos2d::Vec2> Graph::BFS(int srcCol, int srcRow, int desCol, int desRow, CELL_STATE* boardState)
{
	Cell* srcCell = m_Cells[m_Col*srcRow + srcCol];

	//actual path if found
	std::vector<cocos2d::Vec2> finalPath;
	cocos2d::Vec2* path = new cocos2d::Vec2[m_Row*m_Col];
	memset(path, NULL, m_Col*m_Row * sizeof(cocos2d::Vec2));

	//if trace back to the src cell will stop if x and y is -1
	path[m_Col*srcRow + srcCol].x = -1;
	path[m_Col*srcRow + srcCol].y = -1;

	//
	path[m_Col*desRow + desCol].x = desCol;
	path[m_Col*desRow + desCol].y = desRow;

	//path found flag
	bool pathFound = false;

	bool* visitedCell = new bool[m_CellCount];
	memset(visitedCell, false, m_CellCount * sizeof(bool));

	std::queue<Cell*> BFSqueue;
	BFSqueue.push(srcCell);
	visitedCell[m_Col*srcRow + srcCol] = true;

	while (!BFSqueue.empty())
	{
		Cell* currentSrc = BFSqueue.front();
		BFSqueue.pop();


		if (currentSrc->m_Col == desCol && currentSrc->m_Row == desRow)
		{
			pathFound = true;
			break;
		}
		for (std::vector<Cell*>::iterator neighbour = currentSrc->m_Adjacency.begin(); neighbour != currentSrc->m_Adjacency.end(); ++neighbour)
		{
			if (!visitedCell[m_Col*(*neighbour)->m_Row + (*neighbour)->m_Col] && boardState[m_Col*(*neighbour)->m_Row + (*neighbour)->m_Col] == CS_EMPTY)
			{
				visitedCell[m_Col*(*neighbour)->m_Row + (*neighbour)->m_Col] = true;
				BFSqueue.push(*neighbour);
				path[m_Col*(*neighbour)->m_Row + (*neighbour)->m_Col].x = currentSrc->m_Col;
				path[m_Col*(*neighbour)->m_Row + (*neighbour)->m_Col].y = currentSrc->m_Row;
			}
		}
	}


	delete[] visitedCell;

	if (pathFound)
	{
		cocos2d::log("FOUND PATH");
		cocos2d::Vec2 cur = path[m_Col*desRow + desCol];
		finalPath.push_back(cocos2d::Vec2(desCol, desRow));
		while (cur.x != -1 && cur.y != -1)
		{
			finalPath.push_back(cur);
			cur = path[m_Col*(int)cur.y + (int)cur.x];
		}
		std::reverse(finalPath.begin(), finalPath.end());
	}

	delete[] path;

	return finalPath;
}
