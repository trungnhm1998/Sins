#pragma once
#include "Sin.h"
class GlutonyBall :
	public Sin
{
public:
	GlutonyBall();
	~GlutonyBall();


	bool init() override;
	void destroySin() override;
	CREATE_FUNC(GlutonyBall)
};

