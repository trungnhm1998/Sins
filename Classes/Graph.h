#pragma once
#include <queue>
#include "Cell.h"
#include "cocos2d.h"
#include "Sin.h"
#include "Board.h"


class Graph
{
public:
	Graph(unsigned int row, unsigned int col);
	~Graph();
	std::vector<cocos2d::Vec2> BFS(int srcRow, int srcCol,int desRow, int desCol, CELL_STATE* boardState);



private:
	unsigned int m_CellCount;
	unsigned int m_Col;
	unsigned int m_Row;
	Cell** m_Cells;
};

