#pragma once
#include "Sin.h"
class WrathBall :
	public Sin
{
public:
	WrathBall();
	~WrathBall();


	bool init() override;
	void destroySin() override;

	CREATE_FUNC(WrathBall)
};

