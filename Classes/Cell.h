#pragma once
#include <vector>

class Cell
{
public:
	Cell();
	Cell(int row, int col);
	~Cell();
	void addNeighbour(Cell* neighbour);
	int m_Col;
	int m_Row;
	std::vector<Cell*> m_Adjacency;

private:
};

